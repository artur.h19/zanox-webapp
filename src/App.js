import React from "react";
import { Layout } from "antd";

import { Main } from "./containers";

import "./App.css";

function App() {
  return (
    <div className="App">
      <Layout>
        <Main />
      </Layout>
    </div>
  );
}

export default App;
