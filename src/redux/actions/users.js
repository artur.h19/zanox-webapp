import axios from "axios";
import * as types from "../actionsTypes";

function fetchUsersRequest() {
  return {
    type: types.LOAD_USERS,
  };
}

function fetchUsersSuccess(data) {
  return {
    type: types.LOAD_USERS_SUCCESS,
    payload: data,
  };
}

export function fetchUsersFailure(failure) {
  return {
    type: types.LOAD_USERS_FAILURE,
    payload: failure,
  };
}

export function fetchUsers() {
  return async function (dispatch) {
    dispatch(fetchUsersRequest());

    try {
      const { data } = await axios("http://127.0.0.1:3001/api/users");

      dispatch(fetchUsersSuccess(data.users));
    } catch (err) {
      dispatch(fetchUsersFailure(err));
    }
  };
}

export function searchUser(name) {
  return async function (dispatch) {
    try {
      const { data } = await axios(
        `http://127.0.0.1:3001/api/users?name=${name}`
      );
      dispatch({
        type: types.SEARCH_USER,
        payload: data.users,
      });
    } catch (err) {}
  };
}

export function updateUser(id) {
  return async function (dispatch) {
    try {
      const { data } = await axios(`http://127.0.0.1:3001/api/users/${id}`, {
        method: "PUT",
      });
      dispatch({
        type: types.UPDATE_USER,
        payload: data.users,
      });
    } catch (err) {}
  };
}
