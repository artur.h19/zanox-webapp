import * as types from "../actionsTypes";

const initialState = {
  matches: [],
  loading: false,
  error: null,
  searchedUser: [],
};

export function usersReducer(state = initialState, action) {
  switch (action.type) {
    case types.LOAD_USERS:
      return {
        loading: true,
        ...state,
      };
    case types.LOAD_USERS_SUCCESS:
      return {
        error: null,
        loading: false,
        matches: action.payload,
      };
    case types.LOAD_USERS_FAILURE:
      return {
        loading: false,
        matches: [],
        error: action.payload,
      };
    case types.SEARCH_USER: {
      return {
        ...state,
        loading: false,
        searchedUser: action.payload,
        error: null,
      };
    }
    case types.UPDATE_USER: {
      return {
        ...state,
        loading: false,
        matches: action.payload,
        error: null,
      };
    }
    default:
      return state;
  }
}
