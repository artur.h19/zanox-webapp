import { useDispatch } from "react-redux";
import { updateUser } from "../redux/actions/users";

// Styled
import styled from "styled-components";
import { Typography, Row, Col } from "antd";

const { Text } = Typography;

const LiContainer = styled.li`
  padding: 5px;
  font-size: 0.8rem;

  &:hover {
    border-bottom: 1px solid #d9d9d9;
  }
`;

const TimeContainer = styled.div`
  background-color: #1890ff;
  border-radius: 5px;
  text-align: center;
  color: white;
  font-family: Roboto;
  font-size: 0.7rem;
`;

const IconsContainer = styled.i`
  color: #5cdbd3;
  margin-left: 15px;
  cursor: pointer;
  font-size: 1.2rem;
`;

const TextContainer = styled(Text)`
  font-weight: 600;
  font-family: Roboto;
  color: #8c8c8c;
`;

const UserItem = ({ user }) => {
  const dispatch = useDispatch();

  const onDelete = () => {
    dispatch(updateUser(user.id));
  };

  return (
    <LiContainer>
      <Row justify="start">
        <Col span={14}>
          <Row>
            <Col span={8}>
              <TextContainer style={{ fontFamily: "Roboto" }}>
                {user.name}
              </TextContainer>
            </Col>
            <Col span={4}>
              <TimeContainer>
                {new Date().getMinutes() -
                  new Date(user.updatedAt).getMinutes()}{" "}
                Minutes
              </TimeContainer>
            </Col>
          </Row>
        </Col>
        <Col span={8} justify="end">
          <Row justify="end">
            <Col>
              <TextContainer type="disabled" style={{ color: "#5cdbd3" }}>
                START SESSION
              </TextContainer>
            </Col>
            <Col>
              <IconsContainer className="fas fa-comment-dots"></IconsContainer>
              <IconsContainer
                className="fas fa-trash-alt"
                onClick={onDelete}
              ></IconsContainer>
            </Col>
          </Row>
        </Col>
      </Row>
    </LiContainer>
  );
};

export default UserItem;
