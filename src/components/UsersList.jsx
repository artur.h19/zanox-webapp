import styled from "styled-components";
import UserItem from "./UserItem";

const ListContainer = styled.ul`
  padding: 40px;
  width: 100%;
  list-style: none;
`;

const UsersList = ({ users }) => {
  return (
    <ListContainer>
      {users.map((user) => (
        <UserItem key={user.id} user={user} />
      ))}
    </ListContainer>
  );
};

export default UsersList;
