import { useState } from "react";

// Styled
import { Typography, Row, Col, Button, Select } from "antd";
import styled from "styled-components";

// Redux
import { useSelector, useDispatch } from "react-redux";
import { searchUser, updateUser } from "../redux/actions/users";

const { Title } = Typography;
const { Option } = Select;

const HeaderContainer = styled.div`
  padding: 0 30px;
  margin-bottom: 20px;
`;

const ButtonContainer = styled(Button)`
  border-radius: 15px;
  font-weight: 600;
  background-color: #faad14;
  border: none;
`;

const Header = ({ usersLength }) => {
  const [search, setSearch] = useState("");

  const dispatch = useDispatch();
  const searchedUser = useSelector((state) => state.users.searchedUser);

  function onChange(value) {
    setSearch(value);
  }

  function onSearch(val) {
    dispatch(searchUser(val));
  }

  const inviteHandler = () => {
    if (search) {
      dispatch(updateUser(search));
    }
  };

  return (
    <HeaderContainer>
      <Row justify="space-between">
        <Col span={14}>
          <Title
            type="secondary"
            level={2}
            style={{ color: "#5cdbd3", fontFamily: "Roboto" }}
          >
            {usersLength} Attendees in Waiting Room
          </Title>
        </Col>
        <Col span={10}>
          <Row justify="space-between">
            <Col>
              <Select
                showSearch
                style={{ width: 200 }}
                placeholder="Select a person"
                dropdownMatchSelectWidth={false}
                optionFilterProp="children"
                onChange={onChange}
                onSearch={onSearch}
                filterOption={(input, option) =>
                  option.children.toLowerCase().indexOf(input.toLowerCase()) >=
                  0
                }
              >
                {searchedUser &&
                  searchedUser.length &&
                  searchedUser.map((user) => (
                    <Option key={user.name} value={user.id}>
                      {user.name}
                    </Option>
                  ))}
              </Select>
            </Col>
            <Col>
              <ButtonContainer onClick={inviteHandler} type="primary">INVITE STUFF</ButtonContainer>
            </Col>
          </Row>
        </Col>
      </Row>
    </HeaderContainer>
  );
};

export default Header;
