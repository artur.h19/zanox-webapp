import React, { useEffect } from "react";

// Redux
import { useDispatch, useSelector } from "react-redux";
import { fetchUsers } from "../../redux/actions/users";

// Components
import Header from "../../components/Header";
import UserList from "../../components/UsersList";

// Styles
import styled from "styled-components";
import { Divider } from "antd";

const Wrapper = styled.div`
  max-width: 920px;
  margin: 100px auto;
  width: 100%;
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);
  padding: 30px 0;
  border-radius: 10px;
`;

const Main = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchUsers());
  }, [dispatch]);

  const users = useSelector((state) => state.users.matches);
  return (
    <Wrapper>
      <Header usersLength={users.length} />
      <Divider />
      <UserList users={users} />
    </Wrapper>
  );
};

export default Main;
